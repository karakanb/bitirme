import json

from video import VideoProcessor
from normalize import FrameDataFormatter
from correlation import Correlator
import os


def sanitize_file_names(filename):
    filename = filename.replace('/', '--')
    filename = filename.replace('.', '-')
    return filename


class VideoComparator:
    def __init__(self, first_video_path, second_video_path, face_detection_scale, facial_landmark_scale):
        self.first_video_path = first_video_path
        self.second_video_path = second_video_path
        self.facial_landmark_scale = facial_landmark_scale
        self.face_detection_scale = face_detection_scale
        self.cache_directory = 'cached_landmarks'
        self.output_directory = 'correlation_calculations'
        self.first_video_safe_path = sanitize_file_names(self.first_video_path)
        self.second_video_safe_path = sanitize_file_names(self.second_video_path)
        self.correlation_calculations = {}

    def __should_match(self):
        first_name = self.first_video_path.split('/')[-1]
        first_mimic = first_name.split('_')[0]

        second_name = self.second_video_path.split('/')[-1]
        second_mimic = second_name.split('_')[0]

        return first_mimic == second_mimic

    def __is_cached(self, safe_path):
        return os.path.isfile("%s/%s.json" % (self.cache_directory, safe_path))

    def __get_cached(self, safe_path):
        cached_content = {}
        if self.__is_cached(safe_path):
            cached_content = json.load(open("%s/%s.json" % (self.cache_directory, safe_path)))

        return cached_content

    def __cache(self, data, safe_path):
        json_path = "%s/%s.json" % (self.cache_directory, safe_path)
        with open(json_path, 'w') as outfile:
            json.dump(data, outfile)

    def __save_calculations(self, data):
        filename = self.get_safe_filename()

        json_path = "%s/%s" % (self.output_directory, filename)
        with open(json_path, 'w') as outfile:
            json.dump(data, outfile)

    def get_safe_filename(self):
        return "%s_-_%s_-_%d_-_%d_-_%d.json" % (
            self.first_video_safe_path, self.second_video_safe_path, self.face_detection_scale,
            self.facial_landmark_scale,
            self.__should_match())

    def compare(self):

        # Process the first video.
        first_frames = self.__get_cached(self.first_video_safe_path)
        if not first_frames:
            vp = VideoProcessor(self.first_video_path, self.face_detection_scale, self.facial_landmark_scale)
            vp.process()
            first_frames = vp.landmark_list
            self.__cache(first_frames, self.first_video_safe_path)

        # Process the second video.
        second_frames = self.__get_cached(self.second_video_safe_path)
        if not second_frames:
            vp = VideoProcessor(self.second_video_path, self.face_detection_scale, self.facial_landmark_scale)
            vp.process()
            second_frames = vp.landmark_list
            self.__cache(second_frames, self.second_video_safe_path)

        # Format the points of the first video.
        normalizer = FrameDataFormatter(first_frames)
        first_normalized = normalizer.format()

        # Format the points of the second video.
        normalizer = FrameDataFormatter(second_frames)
        second_normalized = normalizer.format()

        # Correlate the first and second data.
        correlator = Correlator(first_normalized, second_normalized)
        self.correlation_calculations = correlator.calculate_correlations()
        self.__save_calculations(self.correlation_calculations)

        return self.correlation_calculations
