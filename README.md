# Mimic Authentication

Bu projeyi kullanmak için önce video dosyaları şu şekilde klasörlenmeli ve adlandırılmalı:

```
- video
    - kisi_adi
        - mimikadi_numara.mov
```

Örnek dağılım şu şekilde görülebilir:
```
- video
    - burak
        - dur_1.mov
        - gulucuk_1.mov
        - gulucuk_2.mov
    - dilanaz
        - dur_1.mov
        - dur_2.mov
        - gulucuk_1.mov
        - gulucuk_2.mov
```

## Koşturmak
Öncelikle tüm videoların hesaplamaları oluşturulmalı.
```
python3 cache_videos.py
```

Ardından hesaplamalar oluşturulmalı:
```
python3 prepare_training_data.py
```

Burdan sonrası machine learning.