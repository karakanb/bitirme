import os
import itertools
import concurrent.futures

from VideoComparator import VideoComparator
from progress_bar import ProgressBar

def get_video_paths(root_path):
    videos = []
    for root, directories, filenames in os.walk(root_path):
        for filename in filenames:
            if filename.endswith('.mov'):
                videos.append(os.path.join(root, filename))

    return videos


def get_existing_calculations(root_path):
    videos = []
    for root, directories, filenames in os.walk(root_path):
        for filename in filenames:
            if filename.endswith('.json'):
                videos.append(filename)

    video_dict = dict.fromkeys(videos, 0)
    return video_dict


def calculate(first_video, second_video):
    vc = VideoComparator(first_video, second_video, 1, 1)
    if vc.get_safe_filename() not in existing_calculations:
        vc.compare()


videos = get_video_paths('video')
combinations = list(itertools.combinations(videos, 2))
existing_calculations = get_existing_calculations('correlation_calculations')
bar = ProgressBar(len(combinations))
print()
bar.init()

# We can use a with statement to ensure threads are cleaned up promptly
with concurrent.futures.ProcessPoolExecutor(max_workers=4) as executor:
    future_to_video = {executor.submit(calculate, video[0], video[1]): video for video in combinations}
    for future in concurrent.futures.as_completed(future_to_video):
        bar.progress()

bar.finish()
print("Successfully calculated %d calculations." % len(combinations))
