import argparse

import sys

from video import VideoProcessor
from normalize import FrameDataFormatter
from correlation import Correlator
import json


def initial_video(input_path, detection_scale, landmark_scale):
    vp = VideoProcessor(input_path, detection_scale, landmark_scale)
    vp.process()


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("original_video", help="path to the input file")
    parser.add_argument("test_video", help="path to the input file")
    parser.add_argument("-ds", "--detection-scale",
                        help="face detection scale, between 0 and 1",
                        default=0.5,
                        type=float)
    parser.add_argument("-ls", "--landmark-scale",
                        help="facial landmark detection scale, between 0 and 1",
                        default=0.5,
                        type=float)

    args = parser.parse_args()

    face_detection_scale = 1
    facial_landmark_scale = 1

    # Process the first video.
    vp = VideoProcessor(args.original_video, face_detection_scale, facial_landmark_scale)
    vp.process()
    first_frames = vp.landmark_list

    # Process the second video.
    vp = VideoProcessor(args.test_video, face_detection_scale, facial_landmark_scale)
    vp.process()
    second_frames = vp.landmark_list

    # Format the points of the first video.
    normalizer = FrameDataFormatter(first_frames)
    first_normalized = normalizer.format()

    # Format the points of the second video.
    normalizer = FrameDataFormatter(second_frames)
    second_normalized = normalizer.format()

    # Correlate the first and second data.
    correlator = Correlator(first_normalized, second_normalized)
    correlation_calculations = correlator.calculate_correlations()

    original_video = args.original_video.replace('/', '--')
    test_video = args.test_video.replace('/', '--')
    json_path = 'output/correlation_comparison_%s_%s_09.json' % (original_video, test_video)

    with open(json_path, 'w') as outfile:
        json.dump(correlation_calculations, outfile)

    print("Successfully printed the correlations results to ", json_path)
