import sys
from PIL import Image, ImageDraw
import face_recognition
import cv2
import os, errno
import numpy
import time
import pprint
import json
import copy
import matplotlib.pyplot as plt
import matplotlib.image as mpimg
import matplotlib
from progress_bar import ProgressBar

matplotlib.use('MacOSX')


class VideoProcessor:
    def __init__(self, input_file_name, face_detection_scale, facial_landmark_scale, print_stats=True,
                 print_progress=True):
        self.print_progress = print_progress
        self.input_file_name = input_file_name
        self.face_detection_scale = face_detection_scale
        self.facial_landmark_scale = facial_landmark_scale
        self.landmark_list = {}
        self.print_stats = print_stats
        self.print_frame_info = not (print_stats and print_progress)

        file_with_extension = self.input_file_name.split('/')[-1]
        file_name = file_with_extension.split('.')[0]
        self.output_file_name = "output/%s_detect-scale%s_landmark-scale%s.mov" % (file_name,
                                                                                   str(self.face_detection_scale),
                                                                                   str(self.facial_landmark_scale))
        self.landmark_list_output_file_name = "output/%s_landmarks_detect-scale%s_landmark-scale%s.json" % \
                                              (file_name, str(self.face_detection_scale),
                                               str(self.facial_landmark_scale))

        self.progress = ProgressBar(0)

    def __set_input_output_streams(self):
        remove_file(self.output_file_name)

        # Open the input movie file
        self.input_movie = cv2.VideoCapture(self.input_file_name)
        self.length = int(self.input_movie.get(cv2.CAP_PROP_FRAME_COUNT))
        self.progress = ProgressBar(self.length)

        # Create an output movie file (make sure resolution/frame rate matches input video!)
        fourcc = cv2.VideoWriter_fourcc(*'XVID')
        fps = self.input_movie.get(cv2.CAP_PROP_FPS)

        self.width = int(self.input_movie.get(3))
        self.height = int(self.input_movie.get(4))
        self.output_movie = cv2.VideoWriter(self.output_file_name, fourcc, fps, (self.width, self.height))

    def __process_image(self):

        # Initialize an empty array to store the list of landmarks.
        landmark_list = {}

        # Loop over all of the frames.
        frame_number = 0
        while True:

            # Grab a single frame of video
            ret, frame = self.input_movie.read()
            frame_number += 1

            # Quit when the input video file ends.
            if not ret:
                break

            # Convert the image from BGR color (which OpenCV uses) to RGB color (which face_recognition uses)
            rgb_frame = frame[:, :, ::-1]
            face_detection_frame = cv2.resize(rgb_frame, (0, 0), fx=self.face_detection_scale,
                                              fy=self.face_detection_scale)

            # Find all the faces and face encodings in the current frame of video
            top, right, bottom, left = locate_face_with_scale(face_detection_frame, self.face_detection_scale)

            if not_all_zero(top, left, bottom, right):
                pil_image, placed_landmarks = detect_landmarks(rgb_frame, top, bottom, left, right,
                                                               self.facial_landmark_scale, 0.2)

                pil_image = draw_face_rectangle(pil_image, top, left, bottom, right)

                if placed_landmarks:
                    landmark_list["frame%d" % frame_number] = placed_landmarks

                frame = numpy.array(pil_image)
                frame = cv2.cvtColor(frame, cv2.COLOR_RGB2BGR)

            # Write the resulting image to the output video file
            self.output_movie.write(frame)
            if self.print_progress:
                self.progress.progress(
                    "Processed frame %d / %d | %s" % (frame_number, self.length, self.input_file_name))
            elif self.print_frame_info:
                print("Processed frame %d / %d | %s" % (frame_number, self.length, self.input_file_name))

        if self.print_progress:
            self.progress.finish()
        return landmark_list

    def process(self):
        execution_start_time = time.time()

        self.__set_input_output_streams()

        if self.print_stats:
            print("Streams are created for: %s" % self.input_file_name)
            print()

        # Start the timer to measure frame processing time and frame rate.
        frame_processing_time = time.time()

        with open(self.landmark_list_output_file_name, 'w+') as outfile:
            self.landmark_list = self.__process_image()
            json.dump(self.landmark_list, outfile)

        execution_end_time = time.time()

        if self.print_stats:
            print()
            print("***************************************************************************************")
            print("Input file: %s" % self.input_file_name)
            print("Output written to: %s" % self.output_file_name)
            print("Landmark list output file: %s." % self.landmark_list_output_file_name)
            print("---------------------------------------------------------------------------------------")
            print("Face detection scale: %.2f" % self.face_detection_scale)
            print("Facial landmark scale: %.2f" % self.facial_landmark_scale)
            print("Execution completed in %.4f seconds." % (execution_end_time - execution_start_time))
            print("Frame loop completed in %.4f seconds." % (execution_end_time - frame_processing_time))
            print("Frame rate: %.2f fps." % round(self.length / (execution_end_time - frame_processing_time), 2))
            print("***************************************************************************************")
            print()

        # All done!
        self.input_movie.release()
        self.output_movie.release()
        cv2.destroyAllWindows()


def remove_file(filename):
    try:
        os.remove(filename)
    except OSError as e:  # this would be "except OSError, e:" before Python 2.6
        if e.errno != errno.ENOENT:  # errno.ENOENT = no such file or directory
            raise  # re-raise exception if a different error occurred


def multiply(list, multiplier):
    for parentIndex, tuples in enumerate(list):
        list[parentIndex] = tuple([z * multiplier for z in tuples])

    return list


def add_coordinate_offsets(my_list, top_offset, left_offset):
    for parentIndex, tuples in enumerate(my_list):
        modified_tuple = tuple([tuples[0] + left_offset, tuples[1] + top_offset])
        my_list[parentIndex] = modified_tuple

    return my_list


def draw_landmarks(face_landmarks, offsets, scale, pil_image):
    facial_features = [
        'chin',
        'left_eyebrow',
        'right_eyebrow',
        'nose_bridge',
        'nose_tip',
        'left_eye',
        'right_eye',
        'top_lip',
        'bottom_lip'
    ]

    placed_landmarks = {}

    d = ImageDraw.Draw(pil_image)
    for facial_feature in facial_features:
        original_landmarks = face_landmarks[facial_feature]
        scaled_landmarks = multiply(original_landmarks, scale)
        placed_landmarks[facial_feature] = copy.deepcopy(scaled_landmarks)
        offset_landmarks = add_coordinate_offsets(scaled_landmarks, offsets['top'], offsets['left'])
        d.line(offset_landmarks, width=2)

    return pil_image, placed_landmarks


def draw_face_rectangle(pil_image, top, right, bottom, left):
    draw = ImageDraw.Draw(pil_image)
    draw.rectangle(((left, top), (right, bottom)), outline=(0, 0, 255))

    return pil_image


def locate_face_with_scale(frame, scale=1.0):
    face_locations = face_recognition.face_locations(frame)

    top, right, bottom, left = [0, 0, 0, 0]
    if len(face_locations) > 0:
        top, right, bottom, left = face_locations[0]
        top *= 1 / scale
        right *= 1 / scale
        bottom *= 1 / scale
        left *= 1 / scale

    return top, right, bottom, left


def not_all_zero(one=1, two=1, three=1, four=1, five=1, six=1):
    return one != 0 and two != 0 and three != 0 and four != 0 and five != 0 and six != 0


def detect_landmarks(frame, top, bottom, left, right, scale, offset):
    frame_height = frame.shape[0]
    frame_width = frame.shape[1]

    top_wo = int(top - offset * frame_height) if int(top - offset * frame_height) > 0 else 0
    bottom_wo = int(bottom + offset * frame_height) if int(
        bottom + offset * frame_height) < frame_height else frame_height
    left_wo = int(left - offset * frame_width) if int(left - offset * frame_width) > 0 else 0
    right_wo = int(right + offset * frame_width) if int(right + offset * frame_width) < frame_width else frame_width

    # Crop the area.
    facial_landmark_frame = frame[top_wo:bottom_wo, left_wo:right_wo]

    # Scale the area.
    facial_landmark_frame = cv2.resize(facial_landmark_frame, (0, 0), fx=scale, fy=scale)

    landmark_offsets = {
        "top": top_wo,
        "bottom": bottom_wo,
        "left": left_wo,
        "right": right_wo,
    }

    face_landmarks_list = face_recognition.face_landmarks(facial_landmark_frame)

    # Create a PIL image from the frame.
    pil_image = Image.fromarray(frame)

    placed_landmarks = {}

    # If landmarks are found, draw those.
    if len(face_landmarks_list) != 0:
        pil_image, placed_landmarks = draw_landmarks(face_landmarks_list[0], landmark_offsets, 1 / scale, pil_image)

    return pil_image, placed_landmarks


if __name__ == '__main__':
    input_file_name = 'video/furkan/tilt_4.mov'
    face_detection_scale = 1
    facial_landmark_scale = 1
    video = VideoProcessor(input_file_name, face_detection_scale, facial_landmark_scale)
    video.process()
