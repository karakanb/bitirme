import json
import pprint
import math

import numpy as np
import pandas as pd
import sys
from movement_detector import MovementDetector
from scipy.interpolate import interp1d

np.seterr(divide='ignore', invalid='ignore')


def get_facial_features():
    facial_features = [
        'chin',
        'left_eyebrow',
        'right_eyebrow',
        'nose_bridge',
        'nose_tip',
        'left_eye',
        'right_eye',
        'top_lip',
        'bottom_lip'
    ]

    return facial_features


class Correlator:
    def __init__(self, first_dataset, second_dataset, scaled_length=100):
        self.first_dataset = first_dataset
        self.second_dataset = second_dataset
        self.scaled_length = scaled_length
        self.calculations = {}

    @staticmethod
    def __standard_correlation(first_array, second_array):
        return np.corrcoef(first_array, second_array)[1, 0]

    @staticmethod
    def __grad_correlation(first_array, second_array):

        gradient_of_first = np.gradient(first_array)
        gradient_of_second = np.gradient(second_array)

        return np.corrcoef(gradient_of_first, gradient_of_second)[1, 0]

    @staticmethod
    def __diff_correlation(first_array, second_array):

        gradient_of_first = np.diff(first_array)
        gradient_of_second = np.diff(second_array)

        return np.corrcoef(gradient_of_first, gradient_of_second)[1, 0]

    @staticmethod
    def __euclidean_correlation(first_array, second_array):
        return np.linalg.norm(first_array - second_array)

    @staticmethod
    def __cosine_similarity(first_array, second_array):
        return np.dot(first_array, second_array.T) / np.linalg.norm(first_array) / np.linalg.norm(second_array)

    @staticmethod
    def __mean_accuracy(first_array, second_array):
        mean_error = np.sum(np.absolute((first_array - second_array) / first_array)) * (1 / first_array.size)
        return 1 - mean_error

    def calculate_correlations(self):
        features = get_facial_features()
        calculations = {}

        for feature in features:
            first_points = self.first_dataset[feature]
            second_points = self.second_dataset[feature]

            for index, point in enumerate(first_points):
                # Get x-y dictionaries for both of the datasets.
                first_point = point
                second_point = second_points[index]

                # Detect movements and scale to the same size.
                first_scaled_x, first_scaled_y = self.__normalize_with_moves(first_point["x"], first_point["y"])
                second_scaled_x, second_scaled_y = self.__normalize_with_moves(second_point["x"], second_point["y"])

                # Create prefixes for calculation indices.
                x_calculation_prefix = "%s_%d_x_" % (feature, index)
                y_calculation_prefix = "%s_%d_y_" % (feature, index)

                # Perform the correlation-like calculations.
                x_calculations = self.__individual_calculations(first_scaled_x, second_scaled_x, x_calculation_prefix)
                y_calculations = self.__individual_calculations(first_scaled_y, second_scaled_y, y_calculation_prefix)

                calculations = {**calculations, **x_calculations}
                calculations = {**calculations, **y_calculations}

        self.calculations = calculations
        return calculations

    def __individual_calculations(self, first_dataset, second_dataset, prefix=""):
        calculations = {
            prefix + "standard_correlation": self.__standard_correlation(first_dataset, second_dataset),
            prefix + "grad_correlation": self.__grad_correlation(first_dataset, second_dataset),
            prefix + "diff_correlation": self.__diff_correlation(first_dataset, second_dataset),
            prefix + "euclidean": self.__euclidean_correlation(first_dataset, second_dataset),
            prefix + "cosine_similarity": self.__cosine_similarity(first_dataset, second_dataset),
            prefix + "mean_accuracy": self.__mean_accuracy(first_dataset, second_dataset),
        }

        return calculations

    def __scale_to_size(self, x_data):
        interpolation_func = interp1d(list(range(len(x_data))), x_data)
        scaled_data = interpolation_func(np.linspace(0, len(x_data) - 1, self.scaled_length))
        return np.array(scaled_data)

    def __normalize_with_moves(self, x_data, y_data):
        md = MovementDetector(x_data, y_data)
        lower_bound, upper_bound = md.step_with_euclidean()

        scaled_x = self.__scale_to_size(x_data[lower_bound:upper_bound])
        scaled_y = self.__scale_to_size(y_data[lower_bound:upper_bound])

        return scaled_x, scaled_y


def main():
    first_frames = json.load(open('output/normalized_dilanaz_landmarks_detect-scale0.9_landmark-scale0.9.json'))
    second_frames = json.load(open('output/normalized_dilanaz_trial_landmarks_detect-scale0.9_landmark-scale0.9.json'))

    correlator = Correlator(first_frames, second_frames)
    best_correlations = correlator.calculate_correlations()

    with open('output/best_correlations.json', 'w') as outfile:
        json.dump(best_correlations, outfile)


if __name__ == '__main__':
    main()
