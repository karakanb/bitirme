import sys
from time import sleep


class ProgressBar:
    def __init__(self, total, bar_length=60):
        self.total = total
        self.count = 0
        self.bar_length = bar_length

    def __print(self, status=''):
        filled_len = int(round(self.bar_length * self.count / float(self.total)))

        percents = round(100.0 * self.count / float(self.total), 1)
        bar = '=' * filled_len + '-' * (self.bar_length - filled_len)

        sys.stdout.write('[%s] %s%s %s\r' % (bar, percents, '%', status))
        sys.stdout.flush()

    def init(self, status=''):
        print()
        self.__print(status)

    def progress(self, status=''):
        self.count += 1
        self.__print(status)

    def finish(self):
        self.count = self.total
        self.__print()
        print()
        print()
