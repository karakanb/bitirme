import csv
import json
import pandas as pd
import time
import sys
import concurrent.futures
from multiprocessing import Manager

from FileListing import FileListing
from progress_bar import ProgressBar


def label(filename_to_label):
    name_part = filename_to_label.split('.')[0]
    return name_part.split('_-_')[-1]


def get_json_as_dict(file_to_retrieve):
    try:
        with open(file_to_retrieve) as f:
            return json.load(f)
    except:
        print("ERROR filename: %s" % file_to_retrieve)
        raise


fl = FileListing('correlation_calculations')
files = fl.get('.json')
print("Files to process: %d" % len(files))
print()

with open(files[0]) as sample_file:
    dict_keys = list(json.load(sample_file).keys())
    dict_keys.append('label')

print("Retrieved dictionary keys.")

with open('training_data.csv', 'w') as td:
    writer = csv.DictWriter(td, fieldnames=list(dict_keys))
    writer.writeheader()

    print("Wrote the header to the CSV file.")

    bar = ProgressBar(len(files))
    bar.init()

    # We can use a with statement to ensure threads are cleaned up promptly
    with concurrent.futures.ProcessPoolExecutor(max_workers=4) as executor:
        future_to_video = {executor.submit(get_json_as_dict, file): file for file in files}
        for future in concurrent.futures.as_completed(future_to_video):
            bar.progress()

            filename = future_to_video[future]
            dict_result = future.result()
            dict_result['label'] = label(filename)
            writer.writerow(dict_result)

bar.finish()
print("Successfully wrote %d rows to the training_data.csv file.")
