import sys
import numpy as np
import json
import random


def get_facial_features():
    facial_features = [
        'chin',
        'left_eyebrow',
        'right_eyebrow',
        'nose_bridge',
        'nose_tip',
        'left_eye',
        'right_eye',
        'top_lip',
        'bottom_lip'
    ]

    return facial_features


class FrameDataFormatter:
    def __init__(self, frame_data):
        self.frame_data = frame_data
        self.normalized_data = {}
        self.features = get_facial_features()
        self.frame_count = len(self.frame_data)
        self.number_of_features = len(self.features)
        self.feature_counts = {}
        self.__construct_normalized_data_structure()

    def __construct_normalized_data_structure(self):

        first_frame = self.frame_data[next(iter(self.frame_data))]
        for feature in self.features:
            self.feature_counts[feature] = len(first_frame[feature])
            self.normalized_data[feature] = []

            for coordinate in first_frame[feature]:
                self.normalized_data[feature].append({
                    "x": [],
                    "y": []
                })

    def format(self):
        for frame_index, frame in self.frame_data.items():
            for feature in self.features:

                if feature not in frame:
                   continue

                # Get the list of coordinates.
                data_points = frame[feature]

                # Set the same data to the arrays.
                for index, coordinate in enumerate(data_points):
                    self.normalized_data[feature][index]["x"].append(coordinate[0])
                    self.normalized_data[feature][index]["y"].append(coordinate[1])

        return self.normalized_data

    def __randomized_access_test(self, iteration_count):
        for i in range(iteration_count):
            # Construct the frame index.
            random_index = random.randint(1, self.frame_count - 1)
            frame_index = "frame%d" % random_index

            # Construct the feature index.
            random_feature_index = random.randint(0, self.number_of_features - 1)
            random_feature_name = self.features[random_feature_index]

            # Retrieve the random frame and the feature.
            random_frame = self.frame_data[frame_index]
            random_feature = random_frame[random_feature_name]

            # Retrieve a random coordinate from the selected feature.
            random_coordinate_index = random.randint(0, len(random_feature) - 1)
            random_coordinate = random_feature[random_coordinate_index]

            # Retrieve the normalized version of the same coordinate.
            normalized_coordinate = self.normalized_data[random_feature_name][random_coordinate_index]
            normalized_x = normalized_coordinate["x"][random_index - 1]
            normalized_y = normalized_coordinate["y"][random_index - 1]

            # If no match, print an error.
            if random_coordinate[0] != normalized_x or random_coordinate[1] != normalized_y:
                print("====== NON MATCHED COORDINATE FOUND ======")
                print(random_coordinate)
                print(normalized_coordinate)
                print("====== NON MATCHED COORDINATE FOUND ======")
                raise ValueError("Non matched coordinates found.")

    def __count_test(self):
        for feature in self.features:
            feature_data = self.normalized_data[feature]
            if len(feature_data) != self.feature_counts[feature]:
                raise ValueError("Feature counts do not match for feature: %s" % feature)

            for feature_coordinate in feature_data:
                if len(feature_coordinate["x"]) != self.frame_count or len(feature_coordinate["y"]) != self.frame_count:
                    raise ValueError("Coordinate count is not equal to frame count for feature: %s" % feature)

    def validate(self):
        print("=== Starting tests ===")
        self.__randomized_access_test(100)
        #self.__count_test()
        print("=== All tests have passed. ===")


def main():
    file_name = 'cached_landmarks/video--furkan--tilt_4-mov.json'
    splitted = file_name.split('/')
    output_file_name = "%s/normalized_%s" % (splitted[0], splitted[1])

    frames = json.load(open(file_name))
    normalizer = FrameDataFormatter(frames)
    normalizer.format()
    normalizer.validate()

    with open(output_file_name, 'w+') as outfile:
        json.dump(normalizer.normalized_data, outfile)


if __name__ == '__main__':
    main()
