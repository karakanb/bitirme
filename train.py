import concurrent.futures

import numpy as np
import pandas as pd

import time
import json

from sklearn.externals import joblib

from sklearn.linear_model import LogisticRegression
from sklearn.metrics import f1_score
from sklearn.svm import SVC
from sklearn import tree
from sklearn.neural_network import MLPClassifier
from sklearn.neighbors import KNeighborsClassifier
from sklearn.ensemble import GradientBoostingClassifier
from sklearn.ensemble import RandomForestClassifier
from sklearn.ensemble import AdaBoostClassifier
from sklearn.naive_bayes import GaussianNB
from xgboost import XGBClassifier
from sklearn.model_selection import train_test_split, cross_val_score, cross_validate
from sklearn.ensemble import VotingClassifier
import random


def get_sample(filename, sample_count):
    n = sum(1 for line in open(filename)) - 1
    skip = sorted(random.sample(range(1, n + 1), n - sample_count))
    return pd.read_csv(filename, skiprows=skip)


def classify_old(classifier_name, classifier, X_train, Y_train, X_test, Y_test):
    total_process_beginning = time.clock()

    print("Starting classifier: %s" % classifier_name)
    t_start = time.clock()
    classifier.fit(X_train, Y_train)
    t_end = time.clock()
    print("Trained the classifier: %s" % classifier_name)

    t_diff = t_end - t_start
    train_score = classifier.score(X_train, Y_train)
    test_score = classifier.score(X_test, Y_test)
    predicted_test_labels = classifier.predict(X_test)
    f1_result = f1_score(Y_test, predicted_test_labels)
    print("Calculated accuracy scores: %s" % classifier_name)

    print("trained {c} in {f:.2f} s".format(c=classifier_name, f=t_diff))
    result = {
        'model_name': classifier_name,
        'model': classifier,
        'train_score': train_score,
        'test_score': test_score,
        'f1_score': f1_result,
        'train_time': t_diff,
    }

    joblib.dump(result['model'], "third_models/%s.pkl" % classifier_name, compress=9)
    with open("third_models/%s.json" % classifier_name, 'w') as json_file:
        json.dump({
            'model_name': classifier_name,
            'train_score': result['train_score'],
            'test_score': result['test_score'],
            'f1_score': result['f1_score'],
            'train_time': round(result['train_time'], 4),
            'total_time': round(time.clock() - total_process_beginning, 4),
        }, json_file)

    return


def classify(classifier_name, classifier, X_data, Y_data):
    print("Started classifier: %s" % classifier_name)
    t_start = time.clock()
    scores = cross_validate(classifier, X_data, Y_data, cv=5,
                            scoring=['accuracy', 'average_precision', 'f1', 'precision', 'recall'],
                            return_train_score=True, n_jobs=-1)
    t_end = time.clock()
    result = {
        'model_name': classifier_name,
        'scores': scores,
        'train_time': round(t_end - t_start, 2),
        'sample_length': len(X_data)
    }

    for key in scores.keys():
        scores[key] = scores[key].tolist()

    with open("third_models/%s.json" % classifier_name, 'w') as json_file:
        json.dump(result, json_file)
        print("JSON created: %s" % classifier_name)

    return


def batch_classify_old(X_train, Y_train, X_test, Y_test):
    """
    This method, takes as input the X, Y matrices of the Train and Test set.
    And fits them on all of the Classifiers specified in the dict_classifier.
    The trained models, and accuracies are saved in a dictionary. The reason to use a dictionary
    is because it is very easy to save the whole dictionary with the pickle module.
    
    Usually, the SVM, Random Forest and Gradient Boosting Classifier take quiet some time to train. 
    So it is best to train them on a smaller dataset first and 
    decide whether you want to comment them out or not based on the test accuracy score.
    """

    dict_models = {}

    # We can use a with statement to ensure threads are cleaned up promptly
    with concurrent.futures.ProcessPoolExecutor(max_workers=4) as executor:
        future_to_video = {executor.submit(classify, name, classifier, X_train, Y_train, X_test, Y_test): name for
                           name, classifier in dict_classifiers.items()}
        for future in concurrent.futures.as_completed(future_to_video):
            filename = future_to_video[future]
            dict_models[filename] = future.result()

    return dict_models


def batch_classify(X_data, Y_data):
    dict_models = {}

    # We can use a with statement to ensure threads are cleaned up promptly
    with concurrent.futures.ProcessPoolExecutor(max_workers=1) as executor:
        future_to_video = {executor.submit(classify, name, classifier, X_data, Y_data): name for
                           name, classifier in dict_classifiers.items()}

    return dict_models


def display_dict_models(dict_models, sort_by='test_score'):
    cls = [key for key in dict_models.keys()]
    test_s = [dict_models[key]['test_score'] for key in cls]
    training_s = [dict_models[key]['train_score'] for key in cls]
    training_t = [dict_models[key]['train_time'] for key in cls]

    df_ = pd.DataFrame(data=np.zeros(shape=(len(cls), 4)),
                       columns=['classifier', 'train_score', 'test_score', 'train_time'])
    for ii in range(0, len(cls)):
        df_.loc[ii, 'classifier'] = cls[ii]
        df_.loc[ii, 'train_score'] = training_s[ii]
        df_.loc[ii, 'test_score'] = test_s[ii]
        df_.loc[ii, 'train_time'] = training_t[ii]

    return df_.sort_values(by=sort_by, ascending=False)


dataset = pd.read_csv('training_data.csv', sep=',')
print("Read the CSV file.")

# dataset = dataset.sample(frac=0.25, random_state=9)
# print("Sampled the dataset to 0.25, length of %d." % len(dataset))

# dataset = get_sample('training_data.csv', 15000)
dataset = dataset.fillna(dataset.mean())
print("Filled the NaN values with column means.")

labels = dataset.iloc[:, -1]
data_points = dataset.iloc[:, :-1]
# X_train, X_test, Y_train, Y_test = train_test_split(data_points, labels, test_size=0.2, random_state=99)
# print("Split the dataset into train and test datasets.")

dict_classifiers = {
    "5_Nearest_Neighbors": KNeighborsClassifier(),
    # "20_Nearest_Neighbors": KNeighborsClassifier(n_neighbors=20),
    # "5_Nearest_Neighbors_Weighted": KNeighborsClassifier(weights='distance'),
    # "20_Nearest_Neighbors_Weighted": KNeighborsClassifier(n_neighbors=20, weights='distance'),
    # "Gradient_Boosting": GradientBoostingClassifier(n_estimators=1000),
    # "Random_Forest": RandomForestClassifier(n_estimators=1000),
    # "eclf": VotingClassifier(
    #    estimators=[('nn', KNeighborsClassifier()), ('gb_clf', GradientBoostingClassifier(n_estimators=1000)),
    #                ('rf', RandomForestClassifier(n_estimators=1000))], voting='soft', weights=[2, 1, 1]),
    # "AdaBoost": AdaBoostClassifier(),
    # "XGBoost": XGBClassifier(),
    # "Neural_Net:": MLPClassifier(),
}

batch_classify(data_points, labels)
print("Trained all the classifiers.")
