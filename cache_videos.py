import concurrent.futures
import json
import os
from threading import Thread
from multiprocessing import Process
from random import randint

import sys

import time

from video import VideoProcessor

cache_directory = 'cached_landmarks'


def chunks(l, n):
    n = max(1, n)
    return (l[i:i + n] for i in range(0, len(l), n))


def sanitize_file_names(filename):
    filename = filename.replace('/', '--')
    filename = filename.replace('.', '-')
    return filename


def get_video_paths(root_path):
    videos = []
    for root, directories, filenames in os.walk(root_path):
        for filename in filenames:
            if filename.endswith('.mov'):
                videos.append(os.path.join(root, filename))

    return videos


def get_file_size(path):
    return os.path.getsize(path)


def cache_video(video_path):
    safe_path = sanitize_file_names(video_path)
    json_path = "%s/%s.json" % (cache_directory, safe_path)
    if os.path.isfile(json_path):
        print("Video already cached: %s" % video_path)
        return

    print("Starting processing: %s" % video_path)

    vp = VideoProcessor(video_path, 1, 1, False, False)
    vp.process()

    with open(json_path, 'w') as outfile:
        json.dump(vp.landmark_list, outfile)
        print("Processed video: %s" % video_path)


# Sort videos by size and chunk them to the groups of 3 items.
all_videos = sorted(get_video_paths('video'), key=get_file_size)

# We can use a with statement to ensure threads are cleaned up promptly
with concurrent.futures.ProcessPoolExecutor(max_workers=4) as executor:
    future_to_video = {executor.submit(cache_video, video): video for video in all_videos}
    for future in concurrent.futures.as_completed(future_to_video):
        video = future_to_video[future]

print("Number of videos available: %d" % len(all_videos))
print("Possible number of combinations: %d" % (len(all_videos) * len(all_videos) / 2))
