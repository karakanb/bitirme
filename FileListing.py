import os


class FileListing:
    def __init__(self, directory):
        self.directory = directory

    def get(self, file_extension=''):
        files = []
        for root, directories, filenames in os.walk(self.directory):
            for filename in filenames:
                if filename.endswith(file_extension):
                    files.append(os.path.join(root, filename))

        return files


if __name__ == '__main__':
    fl = FileListing('img')
    files = fl.get()
    print(files)